FROM openjdk:17.0.1-jdk-slim

ADD ./build/libs/Reminder-0.0.1-SNAPSHOT.jar /app/reminder.jar
ADD ./init.sh /app/init.sh
RUN chmod 755 /app/init.sh

ENTRYPOINT ["/app/init.sh"]