create schema if not exists reminder;

create table if not exists reminder.users
(
    id         bigserial primary key not null,
    username   varchar(30)           not null,
    password   varchar(100)           not null,
    enabled    bool                  not null,
    createDate timestamp with time zone,
    email      varchar(50)           not null,
    telegram   varchar(50)           not null,
    CONSTRAINT username_unique UNIQUE (username)
);
comment on table reminder.users is 'Пользователи';
comment on column reminder.users.id is 'Уникальный идентификатор';
comment on column reminder.users.username is 'Логин пользователя';
comment on column reminder.users.password is 'Хэш пароля';
comment on column reminder.users.createDate is 'Дата создания пользователя';
comment on column reminder.users.email is 'Электронная почта';
comment on column reminder.users.telegram is 'Телеграмм';

create table if not exists reminder.reminder
(
    id          bigserial primary key    not null,
    title       varchar(255)             not null,
    description varchar(4096),
    remind      timestamp with time zone not null,
    user_id     bigint                   not null,
    FOREIGN KEY (user_id)
        REFERENCES reminder.users (id) ON DELETE CASCADE
);
comment on table reminder.reminder is 'Напоминание';
comment on column reminder.reminder.id is 'Уникальный идентификатор';
comment on column reminder.reminder.title is 'Краткое описание';
comment on column reminder.reminder.description is 'Полное описание';
comment on column reminder.reminder.remind is 'Дата и время напоминания в формате ISO';
comment on column reminder.reminder.user_id is 'Идентификатор пользователя';

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint fk_authorities_users foreign key (username) references reminder.users (username)
);