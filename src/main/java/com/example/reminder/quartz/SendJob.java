package com.example.reminder.quartz;

import com.example.reminder.entity.*;
import com.example.reminder.service.*;
import com.example.reminder.service.db.*;
import lombok.*;
import org.quartz.*;
import org.springframework.stereotype.*;

import java.time.*;

/**
 * job для напоминания
 */
@Service
@AllArgsConstructor
public class SendJob implements Job {
    private final ReminderService reminderService;
    private final UserService userService;
    private final Send sendTelegram;

    /**
     * Получает все напоминания за последнюю минуту
     */
    @Override
    public void execute(JobExecutionContext context) {
        try {
            System.out.println("Начало работы Job");
            final var now = OffsetDateTime.now();
            final var allByRemindBetween = reminderService.findAllByRemindBetween(now.minusMinutes(1), now);
            for (ReminderEntity reminderEntity : allByRemindBetween) {
                UserEntity userEntity = userService.findById(reminderEntity.getUserId());
                sendTelegram.sendMessage(userEntity.getTelegram(), reminderEntity.getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
            //запись в лог ошибки
        }
    }
}
