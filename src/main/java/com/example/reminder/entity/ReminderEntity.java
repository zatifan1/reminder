package com.example.reminder.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.*;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "reminder", schema = "reminder", catalog = "postgres")
public class ReminderEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "remind")
    private OffsetDateTime remind;

    @Column(name = "user_id")
    private Long userId;
}
