package com.example.reminder.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users", schema = "reminder", catalog = "postgres")
public class UserEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "createdate")
    private OffsetDateTime createdate;

    @Column(name = "email")
    private String email;

    @Column(name = "telegram")
    private String telegram;

}
