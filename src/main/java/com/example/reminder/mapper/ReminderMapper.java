package com.example.reminder.mapper;

import com.example.reminder.dto.*;
import com.example.reminder.entity.*;
import org.mapstruct.*;

/**
 * Класс маппер для сущности {@link ReminderEntity}
 */
@Mapper(componentModel = "spring")
public interface ReminderMapper {
    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "userId", ignore = true)})
    ReminderEntity dtoToCreateRemindEntity(ReminderCreateDto dto);

    @Mapping(target = "userId", ignore = true)
    ReminderEntity dtoToEditRemindEntity(ReminderDto dto);

    ReminderDto remindEntityToDto(ReminderEntity entity);
}
