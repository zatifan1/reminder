package com.example.reminder.controller;


import com.example.reminder.dto.*;
import com.example.reminder.entity.*;
import com.example.reminder.service.*;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.*;
import jakarta.validation.*;
import jakarta.validation.constraints.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.http.*;
import org.springframework.validation.annotation.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Контроллер для работы с напоминаниями
 */
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("${reminder.base-api-url}")
@Tag(name = "Reminder", description = "Reminder")
public class ReminderController {

    private final ReminderProcessService reminderProcessService;

    /**
     * Создание нового напоминания
     */
    @Operation(
            summary = "Create reminder",
            description = "Create reminder"
    )
    @PostMapping(value = "/reminder/create")
    public ResponseEntity<Boolean> createRemind(@Valid @RequestBody ReminderCreateDto reminderCreateDto) {
        reminderProcessService.reminderCreate(reminderCreateDto);
        return ResponseEntity.ok().build();
    }

    /**
     * Удалить напоминание
     */
    @Operation(
            summary = "Delete reminder",
            description = "Delete reminder"
    )
    @PostMapping(value = "/reminder/delete")
    public ResponseEntity<Void> deleteRemind(@Valid @RequestBody @NotNull Long reminderId) {
        reminderProcessService.reminderDelete(reminderId);
        return ResponseEntity.ok().build();
    }

    /**
     * Изменить напоминание
     */
    @Operation(
            summary = "Edit reminder",
            description = "Edit reminder"
    )
    @PostMapping(value = "/reminder/edit")
    public ResponseEntity<Void> editRemind(@Valid @RequestBody ReminderDto reminderDto) {
        reminderProcessService.reminderEdit(reminderDto);
        return ResponseEntity.ok().build();
    }

    /**
     * Поиск напоминания по названию
     */
    @Operation(
            summary = "Search reminder by title",
            description = "Search reminder by title"
    )
    @GetMapping(value = "/reminder/search/title")
    public ResponseEntity<List<ReminderDto>> searchRemindByTitle(@Valid @RequestParam @NotBlank String title) {
        return ResponseEntity.ok(reminderProcessService.findByTitle(title));
    }

    /**
     * Поиск напоминания по описанию
     */
    @Operation(
            summary = "Search reminder by description",
            description = "Search reminder by description"
    )
    @GetMapping(value = "/reminder/search/description")
    public ResponseEntity<List<ReminderDto>> searchRemindByDescription(@Valid @RequestParam @NotBlank String description) {
        return ResponseEntity.ok(reminderProcessService.findByDescription(description));
    }

    /**
     * Поиск напоминания по названию
     */
    @Operation(
            summary = "Search reminder by date",
            description = "Search reminder by date"
    )
    @GetMapping(value = "/reminder/search/date")
    public ResponseEntity<List<ReminderDto>> searchRemindByDate(@Valid @RequestParam @NotBlank String date) {
        return ResponseEntity.ok(reminderProcessService.findByDate(date));
    }

    /**
     * Поиск напоминания по названию
     */
    @Operation(
            summary = "Search reminder by time",
            description = "Search reminder by time"
    )
    @GetMapping(value = "/reminder/search/time")
    public ResponseEntity<List<ReminderDto>> searchRemindByTime(@Valid @RequestParam @NotBlank String time) {
        return ResponseEntity.ok(reminderProcessService.findByTime(time));
    }

    /**
     * Сортировка напоминания по названию
     */
    @Operation(
            summary = "Sort reminder by title",
            description = "Sort reminder by title"
    )
    @GetMapping(value = "/reminder/sort/title")
    public ResponseEntity<List<ReminderDto>> sortRemindByTitle() {
        return ResponseEntity.ok(reminderProcessService.findAllSortByTitle());
    }

    /**
     * Сортировка напоминания по дате
     */
    @Operation(
            summary = "Sort reminder by datetime",
            description = "Sort reminder by datetime"
    )
    @GetMapping(value = "/reminder/sort/datetime")
    public ResponseEntity<List<ReminderDto>> sortRemindByDateTime() {
        return ResponseEntity.ok(reminderProcessService.findAllSortByDateTime());
    }

    /**
     * Вывод списка напоминаний с пагинацией
     */
    @Operation(
            summary = "Pagination list of reminder",
            description = "Pagination list of reminder"
    )
    @GetMapping(value = "/reminder/list")
    public ResponseEntity<Page<ReminderEntity>> listRemind(Pageable pageable) {
        return ResponseEntity.ok(reminderProcessService.findAllPagination(pageable));
    }
}
