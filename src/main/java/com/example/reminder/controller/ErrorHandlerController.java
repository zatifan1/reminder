package com.example.reminder.controller;

import com.example.reminder.dto.*;
import jakarta.servlet.http.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер для возврата ответа при ошибках
 */
@RestControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(value = {Exception.class})
    private ResponseEntity<ErrorDTO> serverErrorHandler(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(
                ErrorDTO.builder()
                        .url(request.getRequestURI())
                        .text(e.getMessage())
                        .build());
    }


}
