package com.example.reminder.controller;


import com.example.reminder.entity.*;
import com.example.reminder.service.db.*;
import io.swagger.v3.oas.annotations.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер для работы с пользователями
 */
@Hidden
@RestController
@AllArgsConstructor
@RequestMapping("${reminder.base-api-url}")
public class UserController {

    private final UserService service;

    /**
     * Получение всех пользователей
     */
    @PostMapping(value = "/registration")
    public ResponseEntity<Void> registration(@RequestBody UserEntity entity) {
        service.registration(entity);
        return ResponseEntity.ok().build();
    }
}
