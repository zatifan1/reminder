package com.example.reminder.dto;

import lombok.*;

/**
 * Dto с ошибками для ControllerAdvice
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDTO {
    private String url;
    private String text;
}
