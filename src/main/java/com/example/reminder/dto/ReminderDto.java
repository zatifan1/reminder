package com.example.reminder.dto;

import com.fasterxml.jackson.annotation.*;
import io.swagger.v3.oas.annotations.media.*;
import jakarta.validation.constraints.*;
import lombok.*;

import java.time.*;

import static com.fasterxml.jackson.annotation.JsonFormat.Feature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;

/**
 * DTO для Reminder
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Remind", description = "Dto for create remind")
public class ReminderDto {
    @NotNull
    @Schema(description = "Id of reminder", example = "1")
    private Long id;

    @NotNull
    @Schema(description = "Title of reminder", example = "title1")
    private String title;

    @Schema(description = "Description of reminder", example = "description")
    private String description;

    @NotNull
    @Schema(description = "Remind time with timezone", type = "string", example = "2023-01-18T00:05:15.905Z")
    private OffsetDateTime remind;
}
