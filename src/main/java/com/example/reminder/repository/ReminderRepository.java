package com.example.reminder.repository;

import com.example.reminder.entity.*;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.*;

import java.time.*;
import java.util.*;

@Repository
public interface ReminderRepository extends JpaRepository<ReminderEntity, Long> {
    List<ReminderEntity> findByTitleAndUserId(String title, Long userId);

    List<ReminderEntity> findByDescriptionAndUserId(String description, Long userId);

    @Query(value = "select * from reminder.reminder r where cast(remind as date) = cast(:date as date) and user_id = :userId", nativeQuery = true)
    List<ReminderEntity> findByDateAndUserId(@Param("date") String date, @Param("userId") Long userId);

    @Query(value = "select * from reminder.reminder r where cast(remind as time) = cast(:time as time) and user_id = :userId", nativeQuery = true)
    List<ReminderEntity> findByTimeAndUserId(@Param("time") String time, @Param("userId") Long userId);

    @Query("select r from ReminderEntity r")
    Page<ReminderEntity> findByUserId(Long userId, Pageable pageable);

    List<ReminderEntity> findByUserIdOrderByTitle(Long userId);

    List<ReminderEntity> findByUserIdOrderByRemind(Long userId);

    void deleteByIdAndUserId(Long reminderId, Long userId);

    List<ReminderEntity> findAllByRemindBetween(OffsetDateTime dateStart, OffsetDateTime dateFinish);

}
