package com.example.reminder.service;

import org.springframework.stereotype.*;

/**
 * Отправка сообщения в телеграмм
 */
@Service
public class SendTelegram implements Send{
    @Override
    public void sendMessage(String target, String message) {
        //todo отправка сообщения в телеграмм
        System.out.println("Сообщение отправлено в телеграмм пользователю" + target);
    }
}
