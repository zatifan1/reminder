package com.example.reminder.service;

import com.example.reminder.dto.*;
import com.example.reminder.entity.*;
import org.springframework.data.domain.*;

import java.util.*;

public interface ReminderProcessService {

    void reminderCreate(ReminderCreateDto reminderCreateDto);

    void reminderDelete(Long reminderId);

    void reminderEdit(ReminderDto reminderDto);

    List<ReminderDto> findByDescription(String description);

    List<ReminderDto> findByTitle(String name);

    List<ReminderDto> findByDate(String date);

    List<ReminderDto> findByTime(String title);

    List<ReminderDto> findAllSortByTitle();

    List<ReminderDto> findAllSortByDateTime();

    Page<ReminderEntity> findAllPagination(Pageable pageable);
}
