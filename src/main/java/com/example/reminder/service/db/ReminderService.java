package com.example.reminder.service.db;

import com.example.reminder.entity.*;
import org.springframework.data.domain.*;

import java.time.*;
import java.util.*;

public interface ReminderService {
    void save(ReminderEntity entity);

    void delete(Long reminderId, Long userId);

    void edit(ReminderEntity ReminderService);

    List<ReminderEntity> findByDescription(String description, Long userId);

    List<ReminderEntity> findByName(String title, Long userId);

    List<ReminderEntity> findByDate(String date, Long userId);

    List<ReminderEntity> findByTime(String time, Long userId);

    List<ReminderEntity> findAllSortByTitle(Long userId);

    List<ReminderEntity> findAllSortByDateTime(Long userId);

    Page<ReminderEntity> findAllPagination(Pageable pageable, Long userId);

    List<ReminderEntity> findAllByRemindBetween(OffsetDateTime dateStart, OffsetDateTime dateFinish);
}
