package com.example.reminder.service.db.impl;

import com.example.reminder.entity.*;
import com.example.reminder.repository.*;
import com.example.reminder.service.db.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.time.*;
import java.util.*;

@Service
@AllArgsConstructor
public class ReminderServiceImpl implements ReminderService {
    private final ReminderRepository repository;

    @Override
    public void save(ReminderEntity entity) {
        repository.save(entity);
    }

    @Override
    @Transactional
    public void delete(Long reminderId, Long userId) {
        repository.deleteByIdAndUserId(reminderId, userId);
    }

    @Override
    public void edit(ReminderEntity entity) {
        repository.save(entity);
    }

    @Override
    public List<ReminderEntity> findByName(String title, Long userId) {
        return repository.findByTitleAndUserId(title, userId);
    }

    @Override
    public List<ReminderEntity> findByDescription(String description, Long userId) {
        return repository.findByDescriptionAndUserId(description, userId);
    }

    @Override
    public List<ReminderEntity> findByDate(String date, Long userId) {
        return repository.findByDateAndUserId(date, userId);
    }

    @Override
    public List<ReminderEntity> findByTime(String time, Long userId) {
        return repository.findByTimeAndUserId(time, userId);
    }

    @Override
    public List<ReminderEntity> findAllSortByTitle(Long userId) {
        return repository.findByUserIdOrderByTitle(userId);
    }

    @Override
    public List<ReminderEntity> findAllSortByDateTime(Long userId) {
        return repository.findByUserIdOrderByRemind(userId);
    }

    @Override
    public Page<ReminderEntity> findAllPagination(Pageable pageable, Long userId) {
        return repository.findByUserId(userId,pageable);
    }

    @Override
    public List<ReminderEntity> findAllByRemindBetween(OffsetDateTime dateStart, OffsetDateTime dateFinish) {
        return repository.findAllByRemindBetween(dateStart,dateFinish);
    }
}
