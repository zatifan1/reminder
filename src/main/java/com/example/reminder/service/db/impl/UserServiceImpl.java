package com.example.reminder.service.db.impl;

import com.example.reminder.entity.*;
import com.example.reminder.repository.*;
import com.example.reminder.service.db.*;
import lombok.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void registration(UserEntity entity) {
        entity.setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
        entity.setId(null);
        repository.save(entity);
    }

    @Override
    public UserEntity findById(Long userId) {
        return repository.findById(userId).orElseThrow(NoSuchElementException::new);
    }
}
