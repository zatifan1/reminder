package com.example.reminder.service.db;

import com.example.reminder.entity.*;

public interface UserService {
    void registration(UserEntity entity);

    UserEntity findById(Long userId);
}
