package com.example.reminder.service;

public interface Send {
    void sendMessage(String target, String message);
}
