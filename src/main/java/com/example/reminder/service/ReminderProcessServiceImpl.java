package com.example.reminder.service;

import com.example.reminder.dto.*;
import com.example.reminder.entity.*;
import com.example.reminder.mapper.*;
import com.example.reminder.repository.*;
import com.example.reminder.service.db.*;
import lombok.*;
import org.springframework.data.domain.*;
import org.springframework.security.core.context.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;

import java.util.*;

@Service
@AllArgsConstructor
public class ReminderProcessServiceImpl implements ReminderProcessService {
    private final ReminderService reminderService;
    private final ReminderMapper mapper;
    private final UserRepository userRepository;

    @Override
    public void reminderCreate(ReminderCreateDto reminderCreateDto) {
        final var entity = mapper.dtoToCreateRemindEntity(reminderCreateDto);
        entity.setUserId(getUser().getId());
        reminderService.save(entity);
    }

    @Override
    public void reminderDelete(Long reminderId) {
        reminderService.delete(reminderId, getUser().getId());
    }

    @Override
    public void reminderEdit(ReminderDto reminderDto) {
        final var entity = mapper.dtoToEditRemindEntity(reminderDto);
        entity.setUserId(getUser().getId());
        this.reminderService.edit(entity);
    }

    @Override
    public List<ReminderDto> findByTitle(String title) {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findByName(title, getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public List<ReminderDto> findByDescription(String description) {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findByDescription(description, getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public List<ReminderDto> findByDate(String date) {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findByDate(date, getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public List<ReminderDto> findByTime(String title) {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findByTime(title, getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public List<ReminderDto> findAllSortByTitle() {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findAllSortByTitle(getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public List<ReminderDto> findAllSortByDateTime() {
        List<ReminderDto> reminderDtoList = new ArrayList<>();
        for (ReminderEntity reminderEntity : reminderService.findAllSortByDateTime(getUser().getId())) {
            reminderDtoList.add(mapper.remindEntityToDto(reminderEntity));
        }
        return reminderDtoList;
    }

    @Override
    public Page<ReminderEntity> findAllPagination(Pageable pageable) {
        return reminderService.findAllPagination(pageable, getUser().getId());
    }

    private UserEntity getUser() {
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        final var principal = authentication.getPrincipal();
        return userRepository.findByUsername(((User) principal).getUsername());
    }
}
