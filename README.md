Для запуска с помощью docker-compose.yml файла выполните следующие действия в терминале, в папке с dockerfile проекта:

1.`docker build -t zatifan .`

2.Проверим что создался нужный docker image командой `docker images`: Repository-_zatifan_, tag-_latest_

3.Запустим наш проект командой `docker-compose up`

Требования для развертывания проекта:

1. Работающий сервер Postgresql под `localhost:5432`
2. Java 17
3. Docker desktop

После запуска необходимо зарегистрировать пользователя post запросом
`http://localhost:8080/api/v1/registration` тело запроса
`{
"username": "test",
"password": "test",
"enabled": true,
"createdate": "2023-01-19T23:27:12.635Z",
"email": "zakir@mail.ru",
"telegram": "@zakir"
}`

Для удобства проверки можно использовать swagger `http://localhost:8080/swagger-ui/index.html#/` предварительно залогинившись под ранее созданным пользователем